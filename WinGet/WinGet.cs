﻿using System;

namespace WinGet
{
    internal class WinGet
    {
        private static void Main(string[] args)
        {
            var worker = new Worker();
            worker.Run(args);
            Console.WriteLine();
        }
    }
}