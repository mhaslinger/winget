﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using NDesk.Options;

namespace WinGet
{
    internal class Worker
    {
        private const string Hr = "------------------";
        private readonly Dictionary<string, string> _arguments;
        private readonly string _curPath;
        private readonly OptionSet _set;
        private readonly object _writeLock = new object();
        private int _blocksize;
        private long _bytesProcessed;
        private bool _calcHashes;
        private string _filepath;
        private int _lastPercent;
        private bool _measures;
        private Stopwatch _sw;
        private long _totalBytes;

        internal Worker()
        {
            var maxBs = (int) Math.Floor((int.MaxValue - 1)/1024d);
            this._curPath = Environment.CurrentDirectory;
            this._arguments = new Dictionary<string, string>();
            this._set = new OptionSet
                        {
                            {"u=", "the url of the file to retrieve (required)", v => this._arguments.Add("u", v)},
                            {
                                "d=", "the destination directory (optional, default current directory)",
                                v => this._arguments.Add("d", v)
                            },
                            {"p=", "show download progress (optional, default true)", v => this._arguments.Add("p", v)},
                            {"n=", "alternative filename (optional)", v => this._arguments.Add("n", v)},
                            {
                                "c", "calculates md5 and sha1 for downloaded file (optional, default false)",
                                v => this._calcHashes = true
                            },
                            {
                                "b=",
                                string.Format(
                                    "sets the blocksize for each chunk in kb (optional, default 64, max. {0})",
                                    maxBs),
                                v => this._arguments.Add("b", v)
                            },
                            {"h", "displays this message", v => this._displayHelp()}
                        };
        }

        internal void Run(string[] args)
        {
            Console.WriteLine();

            if (args.Length == 0)
            {
                this._displayHelp();
            }
            else if (args.Length == 1 && !_isParamArg(args[0]))
            {
                this._arguments.Add("u", args[0]);
            }
            else
            {
                try
                {
                    this._set.Parse(args);
                }
                catch (Exception)
                {
                    Console.WriteLine("invalid parameter supplied.\n");
                    this._displayHelp();
                }
            }
            if (!this._arguments.ContainsKey("u"))
            {
                Console.WriteLine("no url supplied\n");
                this._displayHelp();
            }
            Uri url = null;
            try
            {
                url = this._arguments["u"].StartsWith("http://", StringComparison.OrdinalIgnoreCase) ||
                      this._arguments["u"].StartsWith("https://", StringComparison.OrdinalIgnoreCase) ||
                      this._arguments["u"].StartsWith("ftp://", StringComparison.OrdinalIgnoreCase)
                    ? new Uri(this._arguments["u"])
                    : new Uri("http://" + this._arguments["u"]);
            }
            catch (ArgumentException)
            {
                Console.WriteLine("url invalid");
                Environment.Exit(1);
            }
            var dest = this._arguments.ContainsKey("d") ? this._arguments["d"] : this._curPath;
            if (dest[dest.Length - 1] != '/' && dest[dest.Length - 1] != '\\')
            {
                dest += "\\";
            }
            var showProgress = true;
            if (this._arguments.ContainsKey("p"))
            {
                try
                {
                    showProgress = bool.Parse(this._arguments["p"]);
                }
                catch (FormatException)
                {
                    Console.WriteLine("progress parameter invalid, defaulting to true");
                }
                catch (ArgumentException)
                {
                    Console.WriteLine("progress parameter invalid, defaulting to true");
                }
            }
            var filename = this._arguments.ContainsKey("n") ? this._arguments["n"] : Path.GetFileName(url.LocalPath);
            this._filepath = dest + filename;
            this._blocksize = 64;
            if (this._arguments.ContainsKey("b"))
            {
                try
                {
                    var kb = int.Parse(this._arguments["b"]);
                    this._blocksize = kb;
                }
                catch (FormatException)
                {
                    Console.WriteLine("supplied blocksize invalid, defaulting to 64");
                }
            }
            var resume = true;
            if (File.Exists(this._filepath))
            {
                Console.WriteLine(
                    "A file with this name already exists in the specified directory - do you want to continue (will override file)? (y/n)");
                var key = Console.ReadKey();
                while (key.KeyChar != 'y' && key.KeyChar != 'n')
                {
                    Console.WriteLine("\nplease try again");
                    key = Console.ReadKey();
                }
                if (key.KeyChar == 'n')
                {
                    resume = false;
                }
                Console.WriteLine("\n{0}", Hr);
            }
            if (resume)
            {
                this._download(url, showProgress);
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("\nexiting");
            }
        }

        private static bool _isParamArg(string s)
        {
            return s.StartsWith("-", StringComparison.OrdinalIgnoreCase);
        }


        private async void _download(Uri url, bool progress)
        {
            Console.WriteLine("\nstarting to download\nfrom: {0} \nto: {1} \nat: {3} with {4}kb per block \n{2}", url,
                this._filepath, Hr, DateTime.Now, this._blocksize);
            this._bytesProcessed = 0L;
            Stream remoteStream = null;
            Stream localStream = null;
            WebResponse response = null;
            try
            {
                using (var md5 = MD5.Create())
                using (var sha1 = SHA1.Create())
                {
                    this._sw = new Stopwatch();
                    this._sw.Start();
                    this._lastPercent = -1;
                    var request = WebRequest.Create(url);
                    response = request.GetResponse();
                    this._measures = true;
                    this._totalBytes = long.Parse(response.Headers["Content-length"] ?? "0");
                    if (this._totalBytes == 0)
                    {
                        Console.WriteLine("can't retrieve total filesize for download.");
                        this._measures = false;
                    }
                    Console.WriteLine("starting to retrieve {0} bytes",
                        this._measures ? this._totalBytes.ToString() : "n.a.");
                    remoteStream = response.GetResponseStream();
                    if (remoteStream == null) return;
                    localStream = File.Create(this._filepath);
                    var bufferSize = this._blocksize*1024;
                    var buffer = new byte[bufferSize];
                    var secondBuffer = new byte[bufferSize];
                    var firstRead = true;
                    var oldBytesRead = 0;
                    while (true)
                    {
                        var bytesRead = await remoteStream.ReadAsync(buffer, 0, bufferSize);
                        if (this._calcHashes)
                        {
                            if (firstRead)
                            {
                                firstRead = false;
                            }
                            else
                            {
                                if (bytesRead == 0)
                                {
                                    md5.TransformFinalBlock(secondBuffer, 0, oldBytesRead);
                                    sha1.TransformFinalBlock(secondBuffer, 0, oldBytesRead);
                                    break;
                                }
                                md5.TransformBlock(secondBuffer, 0, oldBytesRead, null, 0);
                                sha1.TransformBlock(secondBuffer, 0, oldBytesRead, null, 0);
                            }
                            Buffer.BlockCopy(buffer, 0, secondBuffer, 0, bytesRead);
                            oldBytesRead = bytesRead;
                        }
                        else
                        {
                            if (bytesRead == 0)
                            {
                                break;
                            }
                        }
                        localStream.Write(buffer, 0, bytesRead);
                        this._bytesProcessed += bytesRead;
                        if (progress)
                        {
                            this._progressUpdate();
                        }
                    }
                    if (!this._measures)
                    {
                        Console.WriteLine();
                    }
                    this._sw.Stop();
                    var ts = this._sw.Elapsed;
                    var secs = ts.TotalSeconds;
                    var printTime = ts.ToReadableString();
                    ;
                    var ratio = (int) Math.Round((this._bytesProcessed/secs)/1024);
                    var kb = (int) Math.Round(this._bytesProcessed/1024d);
                    lock (this._writeLock)
                    {
                        Console.WriteLine("{3}\nDownload complete. ({0} kb received in {1} - {2} kb/s)", kb, printTime,
                            ratio, Hr);
                        if (this._calcHashes)
                        {
                            Console.WriteLine("\nmd5: {0}\nsha1: {1}", _byteArrToString(md5.Hash),
                                _byteArrToString(sha1.Hash));
                        }
                    }
                    Environment.Exit(0);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("download failed!");
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Environment.Exit(1);
            }
            finally
            {
                if (response != null) response.Close();
                if (remoteStream != null) remoteStream.Close();
                if (localStream != null) localStream.Close();
            }
        }

        private void _progressUpdate()
        {
            lock (this._writeLock)
            {
                if (!this._measures)
                {
                    Console.Write(".");
                    return;
                }
                var progress = this._calcProgress(this._bytesProcessed);
                var print = false;
                if (this._lastPercent == -1)
                {
                    this._lastPercent = 0;
                }
                else if (progress != this._lastPercent)
                {
                    this._lastPercent = progress;
                    print = true;
                }
                if (print)
                {
                    var speed = (int) ((this._bytesProcessed/this._sw.Elapsed.TotalSeconds)/1024);
                    var diff = (100 - progress)/2;
                    var mod = (100 - progress)%2;
                    var dots = "*".Multiply(progress/2);
                    var spaces = " ".Multiply(diff + mod);
                    var cleanupSpaces = " ".Multiply(5);
                    var sb = new StringBuilder(100);
                    sb.Append(dots);
                    sb.Append(spaces);
                    sb.Append("[");
                    sb.Append(progress);
                    sb.Append("%");
                    sb.Append("]");
                    sb.Append(" @ ");
                    sb.Append(speed);
                    sb.Append("kb/s");
                    sb.Append(cleanupSpaces);
                    sb.Append("\r");
                    Console.Write(sb.ToString());
                }
                if (progress != 100) return;
                Console.WriteLine();
            }
        }

        private int _calcProgress(long bytesProcessed)
        {
            var ratio = (double) bytesProcessed/this._totalBytes;
            var percent = ratio*100;
            return (int) percent;
        }

        private static string _byteArrToString(ICollection<byte> bytes)
        {
            var sb = new StringBuilder(bytes.Count);
            foreach (var b in bytes)
            {
                sb.AppendFormat("{0:x2}", b);
            }
            return sb.ToString();
        }

        private void _displayHelp()
        {
            this._set.WriteOptionDescriptions(Console.Out);
            Environment.Exit(0);
        }
    }
}